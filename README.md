Petkov projekt
=========

| header 1 | header 2 |
| -------- | -------- |
| cell 1   | cell 2   |
| cell 3   | cell 4   |

Sometimes you want to be :cool: and add some :sparkles: to your :speech_balloon:. Well we have a :gift: for you:

:exclamation: You can use emoji anywhere GFM is supported. :sunglasses:

You can use it to point out a :bug: or warn about :monkey:patches. And if someone improves your really :snail: code, send them a :bouquet: or some :candy:. People will :heart: you for that.

If you are :new: to this, don't be :fearful:. You can easily join the emoji :circus_tent:. All you need to do is to :book: up on the supported codes.

Consult the [Emoji Cheat Sheet](http://www.emoji-cheat-sheet.com/) for a list of all supported emoji codes. :thumbsup: 


* Inline `code` has `back-ticks around` it.

2. Another item
  * `Unordered sub-list.`
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.  

   Some text that should be aligned with the above item.

* Unordered list can use asterisks
- Or minuses
+ Or pluses

# Next point 

[![pipeline status](https://gitlab.com/Peter-Brtan/petko/badges/master/pipeline.svg)](https://gitlab.com/Peter-Brtan/petko/commits/master)