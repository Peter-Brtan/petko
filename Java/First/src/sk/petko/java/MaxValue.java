package sk.petko.java;

/**
 * Created by peter on 11/19/16.
 */
public class MaxValue {
    public static void main(String[] args){
        byte b=126;
        System.out.println("Byte is:" +b);
        if (b < Byte.MAX_VALUE) {
            b++;
        }
        System.out.println("Byte is:" +b);
    }
}
