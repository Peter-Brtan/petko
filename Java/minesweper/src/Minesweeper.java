import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.applet.*;
import javax.swing.*;


//-----------------< Classes for Minesweeper >--------------//

//* Main Class declares Frame
public class Minesweeper extends Applet implements ActionListener,MouseListener,Runnable
{
    TextField MinesLeft = new TextField(5);
    TextField TimeElapsed = new TextField(5);
    //TextField Status = new TextField(30);
    MineButton NewGame;
    MineButton GameButton[][];
    int Mine[][];
    int Val[][];
    Random Rnd = new Random();
    private int MineLeft = 10;
    int Stat[][];

    public long time = -1;
    Thread t = new Thread(this);
    TextField jf;
    private volatile boolean Running = true;

    public void init()
    {
        NewPanel();
        setLayout(new GridLayout(8,8));
    }

    public void NewPanel()
    {
        NewGame = new MineButton("New",0,0,0);
        GameButton = new MineButton[7][7];
        NewGame.addActionListener(this);
        add(new Label(" "));
        add(new Label(" "));
        //add(Status);
        add(MinesLeft);
        add(NewGame);
        add(TimeElapsed);
        TimeElapsed.setEnabled(false);MinesLeft.setEnabled(false);
        MinesLeft.setText(""+MineLeft+"/10");
        Logic();
        for(int i=0;i<7;i++)
        {
            for(int j=0;j<7;j++)
            {
                GameButton[i][j] = new MineButton(" ",i,j,Val[i][j]);
                GameButton[i][j].addActionListener(this);
                GameButton[i][j].addMouseListener(this);
                add(GameButton[i][j],7*(i) + (j));
            }
        }
        t.start();
    }

    public void CheckWin()
    {
        int flag = 0;
        for(int i=0;i<7;i++)
        {
            for(int j=0;j<7;j++)
            {
                if(Stat[i][j] != Mine[i][j])
                    flag = 1;
            }
        }
        if(flag == 0)
        {
            //Status.setText("Congratulations!! You have done it. You took " + time + " seconds.");
            TimeElapsed.setText("Congratulations!! You have done it. You took " + time + " seconds.");
            showStatus("Congratulations!! You have done it. You took " + time + " seconds.");
            Running = false;
        }
    }

    public void Logic()
    {
        Mine = new int[7][7];
        Val = new int[7][7];
        Stat = new int[7][7];
        for(int i=0;i<7;i++)
        {
            for(int j=0;j<7;j++)
            {
                Mine[i][j] = 0;
                Val[i][j] = 0;
                Stat[i][j] = 0;
            }
        }

        for(int i=0;i<10;i++)
        {
            int r = Rnd.nextInt(7) , c = Rnd.nextInt(7);
            if(Mine[r][c] <= 0)
                Mine[r][c] = 1;
            else
                i--;
        }

        for(int i=0;i<7;i++)
        {
            for(int j=0;j<7;j++)
            {
                if( (i>0) && (j>0) && (Mine[i-1][j-1] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if( (j>0) && (Mine[i][j-1] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if( (i>0) && (Mine[i-1][j] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if( (i<6) && (Mine[i+1][j] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if( (j<6) && (Mine[i][j+1] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if( (i<6) && (j<6) && (Mine[i+1][j+1] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if( (i<6) && (j>0) && (Mine[i+1][j-1] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if( (i>0) && (j<6) && (Mine[i-1][j+1] == 1) )
                    Val[i][j] = Val[i][j] + 1;
                if(Mine[i][j] == 1)
                    Val[i][j] = -1;
            }
        }
    }

    public void actionPerformed(ActionEvent e)
    {
        MineButton mb = (MineButton) e.getSource();
        if(mb.equals(NewGame))
        {
            //Status.setText("Created by Ameya Khasgiwala in 2004. Contact   mail2amey2000@yahoo.com");
            TimeElapsed.setText("Created by Ameya Khasgiwala in 2004. Contact   mail2amey2000@yahoo.com");
            showStatus("Created by Ameya Khasgiwala in 2004. Contact   mail2amey2000@yahoo.com");
        }
        else
        {
            switch(mb.value)
            {
                case -1:
                    mb.setText("ø");
                    //Status.setText("You lost it. Better luck next time. You took  " + time + " seconds.");
                    TimeElapsed.setText("You lost it. Better luck next time. You took  " + time + " seconds.");
                    showStatus("You lost it. Better luck next time. You took  " + time + " seconds.");
                    Running=false;
                    break;
				/*case 0:
					break;*/
                default:
                    mb.setText(""+mb.value);
                    break;
            }
            mb.setEnabled(false);
        }
        CheckWin();
    }

    public void mouseClicked(MouseEvent e)
    {
        MineButton btn = (MineButton) e.getSource();
        if( (e.getButton() == 2) ||(e.getButton() == 3) )
        {
            if(btn.isEnabled() == true)
            {
                btn.setText("M");
                btn.setEnabled(false);
                MinesLeft.setText((--MineLeft)+"/10");
                Stat[btn.row][btn.col] = 1;
            }
            else
            {
                if( (btn.getText()).equals("M") )
                {
                    btn.setText("   ");
                    btn.setEnabled(true);
                    MinesLeft.setText((++MineLeft)+"/10");
                    Stat[btn.row][btn.col] = 0;
                }
            }
        }
        CheckWin();
    }

    public void mouseEntered(MouseEvent e)
    {
    }

    public void mouseExited(MouseEvent e)
    {
    }

    public void mousePressed(MouseEvent e)
    {
    }

    public void mouseReleased(MouseEvent e)
    {
    }

    public void DisableAll()
    {
        for(int i=0;i<7;i++)
        {
            for(int j=0;j<7;j++)
            {
                GameButton[i][j].setEnabled(false);
            }
        }
    }

    public void EnableAll()
    {
        for(int i=0;i<7;i++)
        {
            for(int j=0;j<7;j++)
            {
                GameButton[i][j].setEnabled(true);
            }
        }
    }

    public void run()
    {
        try
        {
            while(Running)
            {
                time++;
                TimeElapsed.setText(time + " sec");
                Thread.sleep(1000);
            }
        }
        catch(InterruptedException e)
        {
            System.out.println("Interrupted");
        }
    }
}



//* Class for MineButtons
class MineButton extends JButton
{
    int row,col;
    int value;

    MineButton(String s,int r,int c,int v)
    {
        super(s);
        row = r;
        col = c;
        value = v;
    }
}