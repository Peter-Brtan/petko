#!/usr/bin/env python3

import os
from datetime import datetime
import sys

file_path = os.path.join(os.environ.get('HOME'),os.path.basename('/tmp/file/file/test01.txt'))
print(file_path)

cur_dir = os.getcwd()
print(f'{cur_dir}')

BASE_DIR = os.path.dirname(__file__) #will get filename and path what was enter from command line 
print(f'{BASE_DIR}')

#os.chdir(BASE_DIR) #change current dir

try:
    os.makedirs('test01/test001')  #create directory and inside dir another dir
except FileExistsError as e:
    print(f'Direcotry \'test01/test001\' already exist')
print(os.listdir())

# os.removedirs('test01/test001') will remove directory/ies

os.rename('test01/test001','test01/test-renamed') #rename dir or file

print(os.stat('test01/test-renamed'))
print('File/Dir has been modified: {}'.format(datetime.fromtimestamp(os.stat('test01/test-renamed').st_mtime)))

for dirpath, dirnames, filenames in os.walk(BASE_DIR):
    print(f'Current Path {dirpath}')
    print(f'Directories {dirnames}')
    print(f'files {filenames}')
    print()


print(os.path.split('/tmp/test.txt'))
print(os.path.exists('/tmp/test.txt'))
print(os.path.isfile('/tmp/test.txt'))
print(os.path.isdir('/tmp/test.txt'))
print(os.path.splitext('/tmp/test.txt'))
print(dir(os.path))

COLORS = {\
"black":"\u001b[38;5;0m",
"red":"\u001b[38;5;196m",
"green":"\u001b[38;5;83m",
"yellow":"\u001b[38;5;220m",
"blue":"\u001b[38;5;27m",
"magenta":"\u001b[38;5;199m",
"white":"\u001b[38;5;231m",
"orange":"\u001b[38;5;208m"
}

def colorText(text):
    for color in COLORS:
        text = text.replace("[["+color+"]]", COLORS[color])
    return text

for i in range(0, 16):
    for j in range(0, 16):
        code = str(i * 16 + j)
        sys.stdout.write(u"\u001b[38;5;" + code + "m " + code.ljust(4))
    print(u"\u001b[0m")

print(u"\u001b[38;5;220mDIR {}".format(os.getcwd()))

print(colorText('[[green]]Toto [[red]]je [[yellow]]nejaky [[white]]text'))
print(colorText('[[blue]]{}[[white]]'.format(dir(os.path))))

print('Normal')
